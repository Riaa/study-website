<?php
require_once('../vendor/autoload.php');
//include('namespace.php');
include('header.php');

?>
    <div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="row">

                <div id="contenbody">
                    <div   style="position:center;  top: 170px; height: 415px; border:solid;  border-color: #a2b9f6; padding:15px;  background-color:#a2b9f6">

                    <div id="homeleft"> <div class="col-md-4">
                            <h2>Mission &amp; Vission</h2>
                            <p>The service of Inchcape is expanding
                                dramatically. We always are there where our customers need us – be it a
                                remote or difficult place. Since our services are ISO certified, our
                                customers can rely on our services. We build long term relationship with
                                our clients. This helps us to get better understanding of clients’
                                demands and at the same time get in-depth knowledge of the maritime
                                industry. </p>

                        </div>
                    </div>

                    <div id="homemid"><div class="col-md-4">
                            <h2>Product &amp; Services</h2>
                            Port Agency is the key factor of our business.
                            Agency services are gradually developed keeping its steady improvement
                            from year to year. Coming from our perfect relations with ports, Customs
                            ,immigrations and other related authorities, we can offer and guaranty
                            excellent service to any ships owner and vessel with reasonable price. <p></p>

                        </div>
                    </div>

                    <div id="homeright"><div class="col-md-4">
                            <h2>About Us</h2>
                            <p>Inchcape Shipping Lines Ltd ( Inchcape –
                                Bangladesh ) and Coscol Shipping Lines Ltd. (Coscol - Bangladesh)  are
                                Global Shipping company, mainly active in the tramp agency business,
                                handling all kind of Bulk, Break bulk, RORO,Container ,Liquid , defence ,
                                UN Cargo,Project cargo &amp; demo vessels. Our longtime experienced
                                operators are able to keep your cost down to the minimum and to protect
                                your interest to the maximum.</p>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="col-md-1"></div>
</br>
</br>
<?php
 include ('footer.php');
 include ('footer_script.php');
?>