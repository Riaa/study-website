-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2018 at 12:13 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hmpilibrary`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `authorname` varchar(50) DEFAULT NULL,
  `softdelete` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `authorname`, `softdelete`) VALUES
(1, 'Nafisa Tabbasum', 'Yes'),
(2, 'jannat', 'Yes'),
(3, 'GUPTO Dash', 'Yes'),
(4, 'Rina', 'No'),
(5, 'Nanci', 'Yes'),
(6, NULL, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `sl` int(11) NOT NULL,
  `id` varchar(12) NOT NULL,
  `B_Title` varchar(60) NOT NULL,
  `B_Pub` varchar(60) NOT NULL,
  `authorid` int(3) NOT NULL,
  `B_PurDate` date NOT NULL,
  `B_Price` int(4) NOT NULL,
  `B_Remark` varchar(255) NOT NULL,
  `B_Rack_No` int(2) NOT NULL,
  `B_Subject_Id` int(3) NOT NULL,
  `deptid` int(3) NOT NULL,
  `available` varchar(3) NOT NULL DEFAULT 'Yes',
  `Activate` int(5) NOT NULL,
  `softdelete` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`sl`, `id`, `B_Title`, `B_Pub`, `authorid`, `B_PurDate`, `B_Price`, `B_Remark`, `B_Rack_No`, `B_Subject_Id`, `deptid`, `available`, `Activate`, `softdelete`) VALUES
(1, '0200101', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'Yes'),
(2, '0200102', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'Yes'),
(3, '0200103', 'INTRODUCTION TO COMPUTERS AND TECHNOLOGY', 'RAM PRASAD & SONS', 1, '2013-05-02', 180, 'NEW BOOK', 1, 2, 0, 'Yes', 1, 'No'),
(4, '0200104', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'Yes'),
(5, '0200105', 'INTRODUCTION TO COMPUTERS AND TECHNOLOGY', 'RAM PRASAD & SONS', 1, '2013-05-02', 180, 'NEW BOOK', 1, 2, 0, 'Yes', 1, 'Yes'),
(6, '0300401', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 1, 'No'),
(7, '0300402', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 0, 'No'),
(8, '0300403', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 1, 'No'),
(9, '0300404', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 1, 'No'),
(10, '0300405', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 1, 'No'),
(11, '0300406', 'PROGRAMMING IN C', 'TMH', 4, '2013-04-02', 150, 'ONE BOOK IN BAD STATE', 2, 3, 0, 'Yes', 1, 'No'),
(12, '0400301', 'PROGRAMMING IN C++', 'TMH', 3, '2013-03-02', 210, 'NEW BOOK', 2, 4, 0, 'Yes', 0, 'No'),
(13, '0400302', 'PROGRAMMING IN C++', 'TMH', 3, '2013-03-02', 210, 'NEW BOOK', 2, 4, 0, 'Yes', 1, 'No'),
(14, '0400303', 'PROGRAMMING IN C++', 'TMH', 3, '2013-03-02', 210, 'NEW BOOK', 2, 4, 0, 'Yes', 1, 'No'),
(15, '0901', 'FUNDAMENTAL OF DS', 'MCGRAW HILL', 0, '2013-05-02', 250, 'NEW BOOK', 3, 9, 0, 'Yes', 1, 'Yes'),
(16, '0902', 'FUNDAMENTAL OF DS', 'MCGRAW HILL', 0, '2013-05-02', 250, 'NEW BOOK', 3, 9, 0, 'Yes', 1, 'No'),
(17, '0903', 'FUNDAMENTAL OF DS', 'MCGRAW HILL', 0, '2013-05-02', 250, 'NEW BOOK', 3, 9, 0, 'Yes', 1, 'No'),
(18, '0100201', 'ASP.NET', 'WROX PUBLICTION', 2, '1998-05-08', 255, 'OLD BOOK', 2, 1, 0, 'Yes', 0, 'No'),
(19, '0100202', 'ASP.NET', 'WROX PUBLICTION', 2, '1998-05-08', 255, 'OLD BOOK', 2, 1, 0, 'Yes', 1, 'No'),
(20, '0600101', 'ENGLISH', 'KAMAL', 1, '2013-05-20', 45, 'NEW BOOK', 2, 6, 0, 'Yes', 1, 'No'),
(21, '0600102', 'ENGLISH', 'KAMAL', 1, '2013-05-20', 45, 'NEW BOOK', 2, 6, 0, 'Yes', 1, 'No'),
(22, '0600103', 'ENGLISH', 'KAMAL', 1, '2013-05-20', 45, 'NEW BOOK', 2, 6, 0, 'Yes', 1, 'No'),
(23, '0300201', 'VISUAL', 'KAMAL', 2, '2013-05-02', 250, 'NEW BOOK', 2, 3, 0, 'Yes', 1, 'No'),
(24, '0300202', 'VISUAL', 'KAMAL', 2, '2013-05-02', 250, 'NEW BOOK', 2, 3, 0, 'Yes', 1, 'No'),
(25, '0100501', 'SPECIAL  ASP.NET', 'WROX PUBLICTION', 5, '2013-05-21', 222, 'ONE BOOK DUE', 3, 1, 0, 'Yes', 1, 'No'),
(26, '0100502', 'SPECIAL  ASP.NET', 'WROX PUBLICTION', 5, '2013-05-21', 222, 'ONE BOOK DUE', 3, 1, 0, 'Yes', 1, 'No'),
(27, '0100503', 'SPECIAL  ASP.NET', 'WROX PUBLICTION', 5, '2013-05-21', 222, 'ONE BOOK DUE', 3, 1, 0, 'Yes', 1, 'No'),
(28, '0601201', 'TEMP', '123PUBLICATION', 12, '2016-04-07', 150, 'NO', 3, 6, 0, 'Yes', 1, 'No'),
(29, '0601202', 'TEMP', '123PUBLICATION', 12, '2016-04-07', 150, 'NO', 3, 6, 0, 'Yes', 1, 'No'),
(30, '0601203', 'TEMP', '123PUBLICATION', 12, '2016-04-07', 150, 'NO', 3, 6, 0, 'Yes', 1, 'No'),
(31, '0601204', 'TEMP', '123PUBLICATION', 12, '2016-04-07', 150, 'NO', 3, 6, 0, 'Yes', 1, 'No'),
(32, '0601205', 'TEMP', '123PUBLICATION', 12, '2016-04-07', 150, 'NO', 3, 6, 0, 'Yes', 1, 'No'),
(33, '1201201', 'JAVA PROGRAMMING', 'BALAGURUSWAUMY', 12, '2016-04-12', 250, 'NEW', 2, 12, 0, 'Yes', 1, 'No'),
(34, '1201202', 'JAVA PROGRAMMING', 'BALAGURUSWAUMY', 12, '2016-04-12', 250, 'NEW', 2, 12, 0, 'Yes', 0, 'No'),
(35, '1201203', 'JAVA PROGRAMMING', 'BALAGURUSWAUMY', 12, '2016-04-12', 250, 'NEW', 2, 12, 0, 'Yes', 1, 'No'),
(36, '1201204', 'JAVA PROGRAMMING', 'BALAGURUSWAUMY', 12, '2016-04-12', 250, 'NEW', 2, 12, 0, 'Yes', 1, 'No'),
(37, '1201205', 'JAVA PROGRAMMING', 'BALAGURUSWAUMY', 12, '2016-04-12', 250, 'NEW', 2, 12, 0, 'Yes', 1, 'No'),
(38, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(39, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(40, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(41, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(42, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(43, '', 'test', 'test', 1, '2018-06-08', 34, '', 2, 2, 0, 'Yes', 1, 'No'),
(44, '', 'aaaaaaaaa', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 2, 'No'),
(45, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 8888, 'No'),
(46, '', '22', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(47, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(48, '', '', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No'),
(49, '', '1', '', 0, '0000-00-00', 0, '', 0, 0, 0, 'Yes', 0, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `cmtbook`
--

CREATE TABLE `cmtbook` (
  `Subject_Code` int(11) NOT NULL,
  `Subject_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `deptname` varchar(50) DEFAULT NULL,
  `softdelete` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `deptname`, `softdelete`) VALUES
(1, 'CMT', 'Yes'),
(2, 'EEE', 'Yes'),
(3, 'CSE', 'Yes'),
(4, 'ENT', NULL),
(5, 'AIDT', NULL),
(6, 'AIDT', NULL),
(7, 'cmt', 'Yes'),
(8, NULL, NULL),
(9, NULL, 'Yes'),
(10, NULL, 'Yes'),
(11, 'EEE2', 'Yes'),
(12, 'CSE', 'No'),
(13, 'EEE2', 'No'),
(14, 'Medical', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `enotes`
--

CREATE TABLE `enotes` (
  `eid` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `subid` int(10) NOT NULL,
  `soft_copy` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enotes`
--

INSERT INTO `enotes` (`eid`, `title`, `subid`, `soft_copy`) VALUES
(1369053778, 'Word', 14, '1369053778.pdf'),
(1369015502, 'Asp.net', 1, '1369015502.pdf'),
(1368992127, 'Control Flow', 3, '1368992127.pdf'),
(1369167400, 'Oracle', 10, '1369167400.doc'),
(1369237334, 'Database Connectivity', 1, '1369237334.pdf'),
(1369237389, 'Database Connectivity', 1, '1369237389.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `issuebook`
--

CREATE TABLE `issuebook` (
  `id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `Book_id` varchar(10) NOT NULL,
  `Issue_date` date NOT NULL,
  `Return_date` date NOT NULL,
  `remarks` varchar(30) NOT NULL,
  `fine` int(3) NOT NULL,
  `Referance_fine` date NOT NULL,
  `Activate` varchar(5) NOT NULL,
  `softdelete` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issuebook`
--

INSERT INTO `issuebook` (`id`, `student_id`, `Book_id`, `Issue_date`, `Return_date`, `remarks`, `fine`, `Referance_fine`, `Activate`, `softdelete`) VALUES
(1, '1', '1', '2018-08-08', '2018-08-16', 'Test', 166, '0000-00-00', 'Yes', 'No'),
(2, '1', '0300401', '2018-08-23', '2018-08-14', 'text\r\n', 0, '0000-00-00', '', 'No'),
(3, '8', '0200104', '2018-08-27', '2018-08-30', 'text', 0, '0000-00-00', '', 'No'),
(4, '9', '0300402', '2018-08-14', '2018-09-01', 'Simple Text\r\n', 0, '0000-00-00', '', 'No'),
(5, '10', '0901', '2018-08-31', '2018-09-08', 'Test', 0, '0000-00-00', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `issue_return`
--

CREATE TABLE `issue_return` (
  `id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `Book_id` varchar(8) NOT NULL,
  `Issue_date` date NOT NULL,
  `Duration` int(2) NOT NULL,
  `Return_date` date NOT NULL,
  `fine` int(11) NOT NULL,
  `Activate` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issue_return`
--

INSERT INTO `issue_return` (`id`, `student_id`, `Book_id`, `Issue_date`, `Duration`, `Return_date`, `fine`, `Activate`) VALUES
(1, 'B00003', '040002', '2013-05-23', 1, '2016-04-02', 84460, '0'),
(2, 'D00002', '070004', '2013-05-19', 1, '2013-05-20', 0, '1'),
(3, 'P00001', '0200101', '2013-05-17', 3, '2013-05-20', 0, '1'),
(4, 'ST00002', '070002', '2013-05-19', 1, '2013-05-20', 0, '1'),
(5, 'B00002', '0400301', '2013-05-17', 5, '2013-05-22', 0, '1'),
(6, 'B00001', '0100202', '2013-05-17', 2, '2013-05-21', 10, '0'),
(7, 'ST00003', '0300402', '2013-05-17', 3, '2013-05-20', 0, '1'),
(8, 'ST00002', '0200102', '2013-05-17', 5, '2013-05-22', 0, '1'),
(9, 'ST00001', '070005', '2013-05-17', 1, '2013-05-18', 0, '1'),
(10, 'ST00001', '0100201', '2013-05-17', 3, '2013-05-20', 0, '0'),
(11, 'B00003', '0400302', '2016-04-02', -16893, '2016-04-02', 84460, '0'),
(12, 'B00006', '0100201', '2016-04-03', 1, '2016-04-04', 1300, '1'),
(13, 'ST00008', '1201202', '2016-04-13', 2, '0000-00-00', 0, '0'),
(14, 'ST00008', '040003', '2016-04-13', 1, '2016-04-14', 0, '0'),
(15, 'B00007', '0400302', '2016-04-13', -6, '2016-04-13', 0, '0'),
(16, 'B00003', '0200103', '2016-04-13', 1, '2016-04-15', 5, '0'),
(17, 'B00007', '0400302', '2016-04-13', 1, '2016-04-13', 0, '0'),
(18, 'B00003', '030001', '2016-06-05', 5, '2016-06-10', 0, '1'),
(19, 'B00008', '1201202', '2016-06-05', 5, '2016-06-10', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `softdelete` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `date`, `description`, `softdelete`) VALUES
(1, '2018-07-18', 'av', 'Yes'),
(2, '2018-07-17', 'java books are not found', 'N'),
(3, '2018-08-20', 'java books are not found', 'No'),
(4, '2018-08-07', 'Some NewBooks are published', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `Note_id` varchar(10) NOT NULL,
  `Subject_name` varchar(50) NOT NULL,
  `B_Rack_no` int(3) NOT NULL,
  `Remark` text NOT NULL,
  `Date` date NOT NULL,
  `Activate` varchar(5) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`Note_id`, `Subject_name`, `B_Rack_no`, `Remark`, `Date`, `Activate`, `type`) VALUES
('040002', 'C++', 2, 'One is bad', '2013-05-19', '1', 'English'),
('040001', 'C++', 2, 'One is bad', '2013-05-19', '1', 'English'),
('080005', 'DBMS', 2, 'One is old', '2013-05-19', '1', 'English'),
('130008', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130007', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130006', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130005', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130004', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130003', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('130002', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('080002', 'DBMS', 1, 'Fresh', '2013-05-17', '1', 'Hindi'),
('080003', 'DBMS', 1, 'Fresh', '2013-05-17', '1', 'Hindi'),
('100001', 'Oracle', 2, 'Four unit', '2013-05-17', '1', 'English'),
('100002', 'Oracle', 2, 'Four unit', '2013-05-17', '1', 'English'),
('100003', 'Oracle', 2, 'Four unit', '2013-05-17', '1', 'English'),
('100004', 'Oracle', 2, 'Four unit', '2013-05-17', '1', 'English'),
('130001', 'Operating system', 2, 'First unit is not include', '2013-05-17', '1', 'English'),
('100005', 'Oracle', 2, 'Four unit', '2013-05-17', '1', 'English'),
('080001', 'DBMS', 1, 'Fresh', '2013-05-17', '1', 'Hindi'),
('070005', 'Digital', 2, 'Fresh', '2013-05-17', '0', 'English'),
('070004', 'Digital', 2, 'Fresh', '2013-05-17', '0', 'English'),
('070003', 'Digital', 2, 'Fresh', '2013-05-17', '1', 'English'),
('070002', 'Digital', 2, 'Fresh', '2013-05-17', '0', 'English'),
('070001', 'Digital', 2, 'Fresh', '2013-05-17', '1', 'English'),
('030001', 'C', 2, 'pankaj sir notes', '2016-04-13', '0', 'English'),
('030002', 'C', 2, 'pankaj sir notes', '2016-04-13', '1', 'English'),
('030003', 'C', 2, 'pankaj sir notes', '2016-04-13', '1', 'English'),
('030004', 'C', 2, 'pankaj sir notes', '2016-04-13', '1', 'English'),
('030005', 'C', 2, 'pankaj sir notes', '2016-04-13', '1', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `page_title` text NOT NULL,
  `description` text NOT NULL,
  `met_tags` text NOT NULL,
  `meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `page_title`, `description`, `met_tags`, `meta_description`) VALUES
(1, 'HOME', 'Home', '<article class=\"col1\">\r\n<div class=\"pad_left1\">\r\n<h2>Welcome to Surya Library</h2>\r\n\r\n<p class=\"font2\">This is repository of Knowleadge.It provide alternative approch for the study <span>And provide quality education to the student</span></p>\r\n\r\n<p><strong>The library collection includes documents in Computer Science, History of Science, light reading materials, Fictions, Stories, General books, Encyclopaedias and Dictionaries, Magazines etc. Research books &amp; Monographs are also included in the collection for labs &amp; research purpose.</strong></p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h2>Individual Approach to Education!</h2>\r\n</div>\r\n\r\n<div class=\"wrapper\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"162\" src=\"http://localhost/libweb/webimg/images/page1_img4.jpg\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>This library give better study material in the exam point of view</strong></p>\r\n\r\n<p>All the hand made notes and pdf study material is made according exam point of view. This includes textbooks as prescribed in the university syllabus and Notes in Hindi/Englis compiled by the computer professional according to the each course .</p>\r\n</div>\r\n</article>\r\n', 'Surya Academy,Library System,Online Library', 'Library include both hindi and english notes'),
(2, 'Courses', 'Courses', '<article class=\"col1\">\r\n<div class=\"pad_left1\">\r\n<h3 class=\"pad_bot1\">BCA</h3>\r\n</div>\r\n\r\n<div class=\"wrapper pad_bot1\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"168\" src=\"http://localhost/libweb/webimg/images/cor2.png\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>This is a three-year full time Degree Course in Computer Applications for students who have passed 10+2.</strong></p>\r\n\r\n<p>The BCA Course was launched for the first time by the university in the year 1996 at the University Teaching Deptt. the year 1996 at the University teaching deptt.The University has hundreds BCA Associate Institutes all over the country. This course is very popular with the student community because of its unique and most advanced curriculum which has been designed exhaustively and experts, conducted by the University. The BCA Course of this University has considered unique among all the BCA Courses being conducted at other Universities in the country.</p>\r\n\r\n<p><b style=\"background:#FF6600;color:#CCFFFF;\">Eligibility:-12<sup>th</sup> in any discipline</b></p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h3 class=\"pad_bot1\">DCA</h3>\r\n</div>\r\n\r\n<div class=\"wrapper pad_bot1\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"168\" src=\"http://localhost/libweb/webimg/images/cor1.png\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>This is a one year (two semester) diploma programmed in computer application for 10+2 candidates.</strong></p>\r\n\r\n<p>this course is suitable for any 10+2 student who wishes to get himself introduction in the field of Computer Application to support the normal discipline or to start his career as Computer Operator, Instructor Programming Assistant.</p>\r\n\r\n<p><b style=\"background:#FF6600;color:#CCFFFF;\">Eligibility:-12<sup>th</sup> in any discipline</b></p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h3 class=\"pad_bot1\">M . Sc (CS)</h3>\r\n</div>\r\n\r\n<div class=\"wrapper pad_bot1\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"166\" src=\"http://localhost/libweb/webimg/images/page2_img1.jpg\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>This is a two year full degree course designed keeping in mind the changing requirements of the students who want to shift from other disciplines to computers.</strong></p>\r\n\r\n<p>While offering appropriate specialization, this course also build the desirable characteristics of a general computing professional, skilled in various aspects of computers. students will be given a complete quality eduction.</p>\r\n\r\n<p><b style=\"background:#FF6600;color:#CCFFFF;\">Eligibility:-</b></p>\r\n<b>*</b> 2 years Master&#39; Degree successfully completing after any discipline of Graduate.<br />\r\n<b>*</b> Lateral Entry into second year After PGDCA</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h3 class=\"pad_bot1\">PGDCA</h3>\r\n</div>\r\n\r\n<div class=\"wrapper pad_bot1\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"168\" src=\"http://localhost/libweb/webimg/images/page3_img3.jpg\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>This is a one year (two semester) Diploma programmed in computer Application and is available to any general graduate.</strong></p>\r\n\r\n<p>This course has been one of the most popular ones in the field of Computer Applications since its inception. Its course structure covers the essential elements of computer programming and application and is of use to any graduate whop wishes to enhance the use of computer in his discipline. This course has also been preferred by graduates willing to start their careers in the field of computer at the &#39;Programmer Level&#39;.</p>\r\n\r\n<p><b style=\"background:#FF6600;color:#CCFFFF;\">Eligibility:-Graduate in any discipline.</b></p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">&nbsp;</div>\r\n</article>\r\n', 'Courses Information,DCA,PGDCA Bhopal', 'dummy description'),
(3, 'Rules and Regulation', 'Rules and Regulation', '<article class=\"col1\">\r\n<div class=\"pad_left1\">\r\n<h2 class=\"pad_bot1\">SCOPE</h2>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>It is an academic library and primarily meant for faculty members, students and staff members of the Institute.</strong></p>\r\nThe library collection includes documents in Computer Science, History of Science, light reading materials, Fictions, Stories, General books, Encyclopaedias and Dictionaries, Magazines etc. Research books &amp; Monographs are also included in the collection for labs &amp; research purpose.</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h2 class=\"pad_bot1\">OPENING HOURS</h2>\r\n\r\n<p class=\"pad_bot1 pad_top2\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>1. Monday to Friday: 9.00 AM &ndash; 4.00 PM</strong></li>\r\n	<li><strong>2. Saturday: Close</strong></li>\r\n	<li><strong>3. Circulation Timings: 9.15 AM &ndash; 1.00 PM and 1.30 PM - 5.15 PM</strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h2 class=\"pad_bot1\">LIBRARY MEMBERSHIP AND PRIVILEGES OF THE MEMBERS</h2>\r\n\r\n<p class=\"pad_bot1 pad_top2\">&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>1. Borrowed books must be returned within/on due date.</strong></li>\r\n	<li><strong>2. The Student ID Card will be used as an issuing Card, maintained for each library member and it will be kept in the library until the student/member return the particulars.</strong></li>\r\n	<li><strong>3. The member must have to present while issuing particulars for other person.</strong></li>\r\n	<li><strong>4. The membership of students/scholars remains valid till the end of their course/fellowship. </strong></li>\r\n	<li><strong>5. Books in bad condition cannot be issued out of the library till they are bound and brought to usable condition.</strong></li>\r\n	<li><strong>6. Reference books,notes and project reports can not be issued</strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n\r\n<div class=\"pad_left1\">\r\n<h2 class=\"pad_bot1\">Overdue/Fine Collection</h2>\r\n\r\n<p class=\"pad_bot1 pad_top2\">&nbsp;</p>\r\n\r\n<ol type=\"1\">\r\n	<li><strong>1. Overright Issued Books- @ Rs.5/- per day.</strong></li>\r\n	<li><strong>2. In case the lost book is out-of-print and widely used, the library committee may recommend extra penalty. </strong></li>\r\n	<li><strong>3. If you do not give fine then fine will be recovered from your caution money</strong></li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n</article>\r\n', 'Rules ,Fine calculation', 'Dummy description'),
(4, 'About us', 'About us', '<article class=\"col1\">\r\n<div class=\"pad_left1\">\r\n<h3 class=\"pad_bot1\">Surya Academy</h3>\r\n</div>\r\n\r\n<div class=\"wrapper pad_bot1\">\r\n<figure class=\"left marg_right1\"><img alt=\"\" height=\"166\" src=\"http://localhost/libweb/webimg/images/colleage.png\" width=\"206\" /></figure>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>Surya Academy is the prestigious and pioneer academy of Bhopal was established in the year 1996 with the objective of providing Quality training and services in the feild of Career Oreinted Courses.</strong></p>\r\n\r\n<p>The Academy develops competent professionals in every field through its training and development courses. Over the years <strong>Surya Academy</strong> has developed as a centre of excellence in the field of computer eduction and offering various govt. recognized courses and other allied services related to computers. Surya Academy as a non profit making organization run by Surya Education and Social Welfare Society and is dedicated to the service of the society.</p>\r\n\r\n<p class=\"pad_bot1 pad_top2\"><strong>Surya Academy has an association with Makanlal Chaturvedi Rashtriya Patrakarita Vishvavidyala, Bhopal</strong> for its Master Degree courses,Bachelor Degree,Post Graduate Diploma and Diploma courses also offers 2 years M.Sc (Computer Science), 3 years Bachelor in Computer Application, 1 year Post Graduate Diploma in Computer Application (PGDCA) and 1 year Diploma in Computer Application.</p>\r\n\r\n<p>For a number of years. Surya Academy students are the best in the <strong>Makhanlal University examinations.</strong>. Student&#39;s result itself narrates the story of the Surya Academy&#39;s Glorious Success.</p>\r\n</div>\r\n</article>\r\n', 'About us,contact us ', 'dummy description'),
(5, 'Contact us', 'Contact Us', ' <article class=\"col1\">\r\n            <div class=\"pad_left1\">\r\n              <h2>Contact US</h2>\r\n              <div style=\"text-align:left; width: 550px;\">\r\n                <table width=\"535\" style=\"border-style: solid; border-width: thin; border-spacing: 0px;\">\r\n\r\n                    <tr style=\"vertical-align: middle;\">\r\n                      <td colspan=\"2\" align=\"center\" bgcolor=\"#006AB4\" height=\"30\" style=\"color:#000000\"><strong>Surya Academy</strong></td>\r\n                    </tr>\r\n                    <tr style=\"vertical-align: middle;\">\r\n                      <td width=\"100\" align=\"left\" bgcolor=\"#40A9E2\" height=\"30\" style=\"color:#000000\">Address :</td>\r\n                      <td width=\"381\"> 124, Zone-II, Maharana Pratap Nagar, Near SBI Bhopal.</td>\r\n                    </tr>\r\n                    <tr style=\"vertical-align: middle;\">\r\n                      <td class=\"style6\" bgcolor=\"#40A9E2\" height=\"30\" style=\"color:#000000\"> Contect No :</td>\r\n                      <td> 0755-2557611, 2575678</td>\r\n                    </tr>\r\n                    <tr style=\"vertical-align: middle;\">\r\n                      <td  bgcolor=\"#40A9E2\">&nbsp;</td>\r\n                      <td height=\"30\"><a href=\"https://maps.google.co.in/maps?q=23.229713,77.436284&amp;num=1&amp;z=17&amp;vpsrc=6&amp;vps=2&amp;hl=en\" >How to reach</a></td>\r\n                    </tr>\r\n                </table>\r\n              </div>\r\n              <p>&nbsp;</p>\r\n            </div>\r\n          </article>', 'contact us,get in touch', 'dummy description');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `project_id` varchar(8) NOT NULL,
  `project_name` varchar(80) NOT NULL,
  `tecnology` varchar(60) NOT NULL,
  `level` varchar(10) NOT NULL,
  `Activate` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `project_name`, `tecnology`, `level`, `Activate`) VALUES
('PR00001', 'Online Library Managment System', 'Php,Mysl', 'BCA', '1'),
('PR00002', 'Result Analysis', 'Asp.Net,MS Access', 'BCA', '1'),
('PR00003', 'Employee Management System', 'Asp.Net,MS Access', 'BCA', '1'),
('PR00004', 'Institute management system', 'VB.Net,MS Access', 'PGDCA', '1'),
('PR00005', 'Banking management system', 'VB.Net,Oracle', 'BCA', '1');

-- --------------------------------------------------------

--
-- Table structure for table `rack`
--

CREATE TABLE `rack` (
  `id` int(3) NOT NULL,
  `Rack_location` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `rack`
--

INSERT INTO `rack` (`id`, `Rack_location`) VALUES
(4, 'Room4'),
(3, 'Room3'),
(2, 'Room1'),
(1, 'Room1'),
(5, 'Room1');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(15) NOT NULL,
  `webtitle` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contactnum` varchar(15) NOT NULL,
  `whyus1` varchar(255) NOT NULL,
  `whyus2` varchar(255) NOT NULL,
  `whyus3` varchar(255) NOT NULL,
  `whyus4` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `googleplus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `webtitle`, `logo`, `country`, `city`, `address`, `email`, `contactnum`, `whyus1`, `whyus2`, `whyus3`, `whyus4`, `facebook`, `twitter`, `linkedin`, `googleplus`) VALUES
(1, 'Library Managment System', '0320576600.jpg', 'Bangladesh', 'Chittagong', 'halishahor,boropul', 'libraryacademy.ac.in', '', 'Healty environment', 'Easy language notes', 'Electronic study material', 'Valuable Qusetion bank', 'www.facebook.com', 'www.twitter.com', 'www.linkedin.com', 'www.googleplus.com');

-- --------------------------------------------------------

--
-- Table structure for table `staff_id`
--

CREATE TABLE `staff_id` (
  `staff_id` varchar(10) NOT NULL,
  `stname` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_id`
--

INSERT INTO `staff_id` (`staff_id`, `stname`) VALUES
('ST00001', 'Shailesh Deshmuk'),
('ST00002', 'Bhupendra Jaiswal '),
('ST00003', 'Bharat kaurav'),
('ST00004', 'Mrs Asma siddiqui'),
('ST00005', 'Pankaj Sir'),
('ST00006', 'Deepak shakya'),
('ST00007', 'aamirstaff'),
('ST00008', 'Tahir Khan'),
('ST00009', 'riita');

-- --------------------------------------------------------

--
-- Table structure for table `stdregist`
--

CREATE TABLE `stdregist` (
  `ID` varchar(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `Enable` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `stdregist`
--

INSERT INTO `stdregist` (`ID`, `username`, `password`, `email`, `Enable`) VALUES
('N3L9TCBJ ', 'aamir', 'e10adc3949ba59abbe56e057f20f883e', 'khanaamir1692@gmail.com', '1'),
('KDH7CCFV', 'afreen', 'aa82265fa31092d327992a21705b1083', 'afreenkhan@gmail.com', '1'),
('VHN3VLSG ', 'azhar', 'e10adc3949ba59abbe56e057f20f883e', 'azhar@gmail.com', '1'),
('9QSWQM23', 'Deepak', '498b5924adc469aa7b660f457e0fc7e5', 'deepak1620@gmail.com', '1'),
('BCRNLCR9 ', 'Girjesh', '827ccb0eea8a706c4c34a16891f84e7b', 'girjesh1254@gmail.com', '1'),
('TFKYWSNE', 'gourav', '9d0a7445f26ddcb89ef2f3ed05cb6380', 'gourav123@gmail.com', '1'),
('AYZ533YD', 'priyesh', 'fe228a6aba6770880098ab424961cc5b', 'priyesh00786@rocketmail.com', '1'),
('AZWN4Z85', 'suraj', 'e10adc3949ba59abbe56e057f20f883e', 'suraj123@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `softdelete` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dob`, `softdelete`) VALUES
(1, 'Yasmin ', '2018-08-05', 'No'),
(2, 'Abid Rafin', '2018-08-01', 'Yes'),
(3, 'Asmana Riaa', '2018-08-20', NULL),
(4, 'Moon Islam', '2018-08-07', NULL),
(5, 'Aratika Islam', '2018-08-08', NULL),
(6, 'Moon ', '2018-08-14', NULL),
(7, NULL, '2018-08-31', NULL),
(8, 'Jorina begum', '2018-08-21', 'No'),
(9, 'Asmana Riaa', '2018-08-20', 'No'),
(10, 'Aratika Islam', '2018-08-08', 'No'),
(11, 'Abid Infas', '2018-08-02', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `studentid`
--

CREATE TABLE `studentid` (
  `ID` varchar(10) NOT NULL,
  `Activate` varchar(1) NOT NULL,
  `sname` varchar(60) NOT NULL,
  `Account_no` varchar(12) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `studentid`
--

INSERT INTO `studentid` (`ID`, `Activate`, `sname`, `Account_no`, `Date`) VALUES
('9QSWQM23 ', '1', 'DEEPAK SHAKYA', 'B00003', '2013-05-17'),
('AYZ533YD ', '1', 'PRIYESH PAL', 'B00002', '2013-05-17'),
('AZWN4Z85 ', '1', 'SURAJ', 'B00008', '2016-06-05'),
('BCRNLCR9 ', '1', 'GIRJESH SHAKYA', 'B00005', '2013-05-19'),
('F9BYDQ7Q ', '0', 'VIVEK RAJPOOT', 'P00003', '2013-05-17'),
('FTQVFWUA ', '0', 'SHUBHAM RAI', 'P00004', '2013-05-17'),
('KDH7CCFV ', '1', 'MANOJ TIWARI', 'M00002', '2013-05-17'),
('N3L9TCBJ ', '1', 'AAMIR KHAN', 'B00001', '2013-05-17'),
('Q6LZZTQL ', '0', 'SUNENDRA LODHI', 'D00002', '2013-05-20'),
('RLP8F3JT ', '0', 'ARSHAD', 'B00007', '2016-04-13'),
('TFKYWSNE ', '1', 'GOURAV GIDWANI', 'P00001', '2013-05-17'),
('USTVR4HZ ', '0', 'ABHISHEK NAYAR', 'D00002', '2013-05-17'),
('VHN3VLSG ', '1', 'AZHAR', 'B00006', '2016-04-03'),
('VS7RNWB2 ', '0', 'AJAY SINGH', 'B00004', '2013-05-17'),
('YSJT6W3H ', '0', 'AFREEN KHAN', 'M00001', '2013-05-17'),
('ZETFCDFR ', '0', 'AMAN QURESHI', 'P00002', '2013-05-17');

-- --------------------------------------------------------

--
-- Table structure for table `student_fine`
--

CREATE TABLE `student_fine` (
  `student_id` varchar(10) NOT NULL,
  `Date` date NOT NULL,
  `Pay_fine` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_fine`
--

INSERT INTO `student_fine` (`student_id`, `Date`, `Pay_fine`) VALUES
('b00001', '2013-05-23', 5),
('b00003', '2016-04-02', 168920),
('b00003', '2016-04-15', 5);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(3) NOT NULL,
  `subject_name` varchar(50) NOT NULL,
  `deptid` int(3) NOT NULL,
  `softdelete` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `subject_name`, `deptid`, `softdelete`) VALUES
(1, 'computer system', 0, 'Yes'),
(2, 'Fundamental of Computer', 0, 'Yes'),
(3, 'C', 0, 'No'),
(4, 'C++', 0, 'No'),
(5, 'C#', 0, 'No'),
(6, 'English', 0, 'No'),
(7, 'Digital', 0, 'No'),
(8, 'DBMS', 0, 'No'),
(9, 'Data Structure', 0, 'No'),
(10, 'Oracle', 0, 'No'),
(11, 'VB.Net', 0, 'No'),
(12, 'Java', 0, 'No'),
(13, 'Operating system', 0, 'No'),
(14, 'PC-Package', 0, 'No'),
(15, 'Principal of management', 0, 'No'),
(16, 'TOC', 0, 'No'),
(123462, 'Computer  system software', 0, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(70) NOT NULL,
  `type` varchar(15) NOT NULL,
  `Enable` varchar(2) NOT NULL,
  `email_verified` varchar(30) NOT NULL DEFAULT 'Yes',
  `soft_delete` varchar(3) DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `username`, `password`, `email`, `type`, `Enable`, `email_verified`, `soft_delete`) VALUES
(9, 'tazia', 'eva', 'tazia', 'eva', 'tizia@gmail.com', 'Staff', '', 'Yes', 'Yes'),
(11, 'asmanaa', 'ria', 'asmana', 'd88dae2df93bbdd975c5129d915ce115', 'asmana@gmail.com', 'Admin', '', 'Yes', 'Yes'),
(12, 'Kazi', 'Sala Uddin', 'admin', '1236', 'amisalabir@gmail.com', 'Admin', '', 'Yes', 'Yes'),
(13, 'asmana', 'ria', 'asaa', 'd88dae2df93bbdd975c5129d915ce115', 'asmanariaa@gmail.com', 'Staff', '', 'Yes', 'Yes'),
(16, 'Mahamuda', 'Akter', 'mem', 'asmana', 'mahamuda@gmail.com', '', '', 'Yes', 'Yes'),
(17, 'Asmana', 'Riaaa', 'ria', 'asmana', 'ria@gmail.com', '', '', 'Yes', 'Yes'),
(18, 'Rokeya', 'rokeya', 'Akter', '1234', 'rokeya@gmail.com', '', '', 'Yes', 'Yes'),
(19, 'Tazia', 'Tasrin', 'Tasrin', 'az', 'Tazia@gmail.com', '', '', 'Yes', 'Yes'),
(20, 'meria', 'Rina', 'Akter', 'asmana', 'rina@gmail.com', '', '', 'Yes', 'Yes'),
(21, 'Ayeshaa', 'ayesha', 'Akter', 'az', 'as@gmail.com', '', '', 'Yes', 'Yes'),
(22, 'Ayeshaa', 'ayesha', 'Akter', 'asmana', 'a@gmail.com', '', '', 'Yes', 'Yes'),
(23, 'asmana', 'Asmanaa', 'Aratika', 'a', 'aratika@gmail.com', '', '', 'Yes', 'No'),
(24, 'Asmana', 'ayesha', 'Aratika', 'asmana', 'aa@gmail.com', '', '', 'Yes', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `writer`
--

CREATE TABLE `writer` (
  `id` int(3) NOT NULL,
  `writer_name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `writer`
--

INSERT INTO `writer` (`id`, `writer_name`) VALUES
(7, 'Dr. S. P. Gupta'),
(6, 'Peter Juliff'),
(5, 'S.K.Basandra'),
(4, 'E Balaguruswamy'),
(3, 'Rajeev Mathur'),
(2, 'Alexis Leon'),
(1, 'Anurag Seetha'),
(8, 'Bartee'),
(9, 'Herber Shield'),
(10, 'Rashid Shiek'),
(11, 'A.Mansoor'),
(12, 'Koontz'),
(13, 'R.K. Shukla'),
(14, 'DR. D.J. Shukla'),
(15, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`sl`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issuebook`
--
ALTER TABLE `issuebook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issue_return`
--
ALTER TABLE `issue_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `rack`
--
ALTER TABLE `rack`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stdregist`
--
ALTER TABLE `stdregist`
  ADD PRIMARY KEY (`username`),
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentid`
--
ALTER TABLE `studentid`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `writer`
--
ALTER TABLE `writer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `sl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `issuebook`
--
ALTER TABLE `issuebook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `issue_return`
--
ALTER TABLE `issue_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rack`
--
ALTER TABLE `rack`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123463;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `writer`
--
ALTER TABLE `writer`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `stdregist`
--
ALTER TABLE `stdregist`
  ADD CONSTRAINT `stdregist_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `studentid` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
